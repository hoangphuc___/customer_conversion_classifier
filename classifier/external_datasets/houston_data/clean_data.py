import json

import pandas as pd

DATA_FILE = 'raw_data.json'
COLUMNS = ['Biết Houston123 Như Thế Nào', 'Họ Và Tên', 'Họ Và Tên (NT1)', 'Họ Và Tên (NT2)', 'Học Lực Đầu Vào', 'Lớp',
           'Nghề Nghiệp (NT1)', 'Nghề Nghiệp (NT2)', 'Ngày Nhập Học', 'Ngày Sinh', 'Số Điện Thoại',
           'Số Điện Thoại (NT1)', 'Số Điện Thoại (NT2)', 'Tên Cơ Sở', 'Tên Trường', 'Địa Chỉ']

USED_COLUMNS = ['Biết Houston123 Như Thế Nào', 'Học Lực Đầu Vào', 'Lớp', 'Nghề Nghiệp (NT1)', 'Nghề Nghiệp (NT2)',
                'Ngày Sinh', 'Tên Cơ Sở', 'Tên Trường', 'Địa Chỉ']

UNUSED_COLUMNS = set(COLUMNS).difference(set(USED_COLUMNS))

with open(DATA_FILE, mode='r', encoding='utf-8') as f:
    data = json.load(f)

df = pd.DataFrame(data)
unused_columns_dropped_df = df.drop(UNUSED_COLUMNS, axis=1)
unused_columns_dropped_df['Ngày Sinh'].astype('datetime64[ns]', errors='ignore')
unique_values = {column_name: unused_columns_dropped_df[column_name].unique().tolist() for column_name in USED_COLUMNS}
career = set(unique_values['Nghề Nghiệp (NT1)']).union(unique_values['Nghề Nghiệp (NT2)'])

ten_co_so_groups = unused_columns_dropped_df.groupby(['Tên Cơ Sở', 'Biết Houston123 Như Thế Nào'])[
    'Học Lực Đầu Vào'].count()
pass
