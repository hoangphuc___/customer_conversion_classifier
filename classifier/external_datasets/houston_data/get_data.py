import json
from time import sleep

import requests
from loguru import logger

DATA_FILE = 'raw_data.json'

next_page_url = 'http://houston123.xyz/api/data-training'

data = []
i = 1
while next_page_url is not None:
    logger.info('Getting page: %s' % i)

    while True:
        response = requests.get(next_page_url)

        if response.status_code != 200:
            sleep(10)
        else:
            break

    json_data = response.json()
    data.extend(json_data['data'])
    next_page_url = json_data['next_page_url']

    i += 1

with open(DATA_FILE, mode='w', encoding='utf-8') as f:
    json.dump(data, f)
