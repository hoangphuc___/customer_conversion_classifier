import json

from loguru import logger
from sklearn.metrics import accuracy_score, precision_score, recall_score, f1_score

from classifier.utils import without_keys
from .preprocess import preprocess_data


def evaluate_prediction(prediction, truth):
    acc = accuracy_score(truth, prediction)
    prec = precision_score(truth, prediction, average='macro')
    rec = recall_score(truth, prediction, average='macro')
    f1 = f1_score(truth, prediction, average='macro')

    results = {
        'accuracy': acc,
        'precision': prec,
        'recall': rec,
        'f1': f1
    }
    return results


def evaluate_model(model, columns_order, label_column, json_file=None, mongodb_cursor=None):
    if json_file is None and mongodb_cursor is None:
        raise Exception("Either 'input_csv' or 'mongodb_cursor' must be set")
    if json_file is not None and mongodb_cursor is not None:
        raise Exception("Usage of both 'input_csv' or 'mongodb_cursor' is not allowed")

    clf = model['classifier']

    if mongodb_cursor is not None:
        logger.info('Retrieving dataset from MongoDB')
        test = list(mongodb_cursor)
    else:
        with open(json_file, mode='r') as f:
            test = json.load(f)

    y_test = [sample[label_column] for sample in test]
    x_test = [without_keys(sample, [label_column]) for sample in test]
    x_test = preprocess_data(
        x_test,
        scaler=model['scaler'],
        dummy_columns=model['dummy_columns'],
    )
    x_test = x_test[columns_order]

    y_pred = clf.predict(x_test)
    result = evaluate_prediction(y_pred, y_test)
    return result
