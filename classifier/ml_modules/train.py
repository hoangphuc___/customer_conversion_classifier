import json
import os

from joblib import dump
from loguru import logger
from sklearn.svm import SVC

from .preprocess import preprocess_data
from ..utils import is_empty_dir, clear_directory, timeit, without_keys


@timeit
def train_model(model_output_dir, columns_order, label_column, json_file=None, mongodb_cursor=None,
                overwrite_model=True):
    logger.info('Training model')
    if json_file is None and mongodb_cursor is None:
        raise Exception("Either 'json_file' or 'mongodb_cursor' must be set")
    if json_file is not None and mongodb_cursor is not None:
        raise Exception("Usage of both 'json_file' or 'mongodb_cursor' is not allowed")

    if mongodb_cursor is not None:
        logger.info('Retrieving dataset from MongoDB')
        train = list(mongodb_cursor)
    else:
        with open(json_file, mode='r') as f:
            train = json.load(f)

    try:
        if not is_empty_dir(model_output_dir):
            if overwrite_model is None:
                user_input = input('%s is not empty. Overwrite? (y/n)' % model_output_dir)
            else:
                if overwrite_model:
                    user_input = 'y'
                else:
                    user_input = 'n'

            if user_input == 'y':
                clear_directory(model_output_dir)
            else:
                return
    except FileNotFoundError:
        os.makedirs(model_output_dir)

    y_train = [sample[label_column] for sample in train]
    x_train = [without_keys(sample, [label_column]) for sample in train]
    x_train = preprocess_data(
        x_train,
        output_scaler_path=model_output_dir + '/scaler.joblib',
        output_dummy_columns_path=model_output_dir + '/dummy_columns.joblib'
    )
    x_train = x_train[columns_order]

    # clf = LinearSVC()
    clf = SVC(kernel='linear', probability=True)
    clf.fit(x_train, y_train)

    dump(clf, model_output_dir + '/classifier.joblib')
