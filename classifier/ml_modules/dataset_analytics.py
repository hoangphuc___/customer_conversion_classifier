from multiprocessing.pool import Pool

import pandas as pd
import tqdm

from .preprocess import process_sample

SCORE_RANGE = {
    'ranges': {
        (None, 3.5): '3.5-',
        (3.5, 5): '3.5-5',
        (5, 6.5): '5-6.5',
        (6.5, 8): '6.5-8',
        (8, None): '8+',
    },
    'continuous': True,
}

field_ranges = {
    'age': {
        'ranges': {
            (None, 16): '16-',
            (17, 24): '17-24',
            (25, 34): '25-34',
            (35, 44): '35-44',
            (45, 54): '45-54',
            (55, 64): '55-64',
            (65, None): '65+',
        },
        'continuous': False,
    },
    'income': {
        'ranges': {
            (None, 30): '30-',
            (30, 60): '30-60',
            (60, 90): '60-90',
            (90, 120): '90-120',
            (120, None): '120+',
        },
        'continuous': True,
    },
    'children_age': {
        'ranges': {
            (None, 6): '5-',
            (6, 11): '6-10',
            (11, 15): '11-14',
            (15, 18): '15-17',
            (18, None): '18+',
        },
        'continuous': True,
    },
    'children_grade': {
        'ranges': {
            (None, 6): '1-5',
            (6, 10): '6-9',
            (10, None): '10-12',
        },
        'continuous': True,
    },
    'children_subject_scores_chemistry': SCORE_RANGE,
    'children_subject_scores_physic': SCORE_RANGE,
    'children_subject_scores_math': SCORE_RANGE,
    'children_subject_scores_literature': SCORE_RANGE,
    'children_subject_scores_english': SCORE_RANGE,
}


def analyze_dataset(data):
    chunksize = 100
    with Pool() as pool:
        data = list(
            tqdm.tqdm(
                pool.imap(
                    process_sample,
                    data,
                    chunksize=chunksize
                ),
                total=len(data),
                desc='Un-nesting data'
            )
        )
    data = pd.DataFrame(data)
    data = data.loc[data['conversion'] == True]
    data = data.drop('conversion', axis=1)

    for field_name, v in field_ranges.items():
        continuous = v['continuous']
        ranges = v['ranges']
        data[field_name] = data[field_name].apply(lambda age: value_to_range(age, ranges, continuous))

    marketing_types = set(data['marketing_type'].tolist())
    marketing_types_count = dict()

    marketing_types_sum_count = data['marketing_type'].value_counts()
    marketing_types_sum_count = marketing_types_sum_count / marketing_types_sum_count.sum()
    marketing_types_sum_count = marketing_types_sum_count.to_dict()
    for marketing_type in marketing_types:
        marketing_type_count = dict()
        marketing_types_count[marketing_type] = marketing_type_count

        marketing_type_count['summary'] = marketing_types_sum_count[marketing_type]
        marketing_type_count['columns'] = dict()

        marketing_type_df = data.loc[data['marketing_type'] == marketing_type]
        marketing_type_count_df = marketing_type_df.apply(pd.value_counts)
        marketing_type_count_df = marketing_type_count_df.drop('marketing_type', axis=1)
        column_names = marketing_type_count_df.columns

        for column_name in column_names:
            marketing_type_column_count_series = marketing_type_count_df[column_name].dropna()
            marketing_type_column_count_series = marketing_type_column_count_series \
                                                 / marketing_type_column_count_series.sum()
            marketing_type_column_count_dict = marketing_type_column_count_series.to_dict()

            marketing_type_count['columns'][column_name] = marketing_type_column_count_dict
    return marketing_types_count


def value_to_range(value, ranges, continuous):
    range = list(filter(lambda range: is_in_range(value, range, continuous), ranges.keys()))
    range = range[0]
    range_string = ranges[range]
    return range_string


def is_in_range(value, range, continuous_range):
    lower_bound = range[0]
    upper_bound = range[1]

    if continuous_range:
        if lower_bound is not None:
            if value < lower_bound:
                return False

        if upper_bound is not None:
            if value >= upper_bound:
                return False
    else:
        if lower_bound is not None:
            if value < lower_bound:
                return False

        if upper_bound is not None:
            if value > upper_bound:
                return False

    return True
