import json
import os
import random
import subprocess

import pandas as pd
from loguru import logger
from pathos.multiprocessing import ProcessingPool
from pymongo import MongoClient
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import MinMaxScaler

from .preprocess import preprocess_data
from ..utils import get_module_dir, timeit

MODULE_DIR = get_module_dir(__file__)

DATASETS_DIR = MODULE_DIR + '/../external_datasets'

GENDERS = ['male', 'female']
SCORE_RANGE = [i * 0.25 for i in range(10 * 4 + 1)]

def generate_dataset_from_existing_datasets(dataset_dir):
    bank = pd.read_csv(
        '%s/bank-additional/bank-additional-full.csv' % dataset_dir, sep=';')
    mall_customers = pd.read_csv('%s/Mall_Customers.csv' % dataset_dir)
    bigml = pd.read_csv('%s/bigml_5c06376d00a1e50a060001dd.csv' % dataset_dir)

    mall_customers = mall_customers[['Genre', 'Age', 'Annual Income (k$)']]
    mall_customers.columns = ['gender', 'age', 'income']
    mall_customers['gender'] = mall_customers['gender'].str.lower()

    min_income = mall_customers['income'].min()
    max_income = mall_customers['income'].max()
    income_group_by_age_gender = mall_customers.groupby(['age', 'gender'])
    income_group_by_age_gender = {name: list(group['income']) for name, group in income_group_by_age_gender}

    bank = bank[['age', 'job', 'marital', 'education']]
    bank.loc[bank.education == 'professional.course', 'education'] = 'tertiary'
    bank.loc[bank.education == 'high.school', 'education'] = 'high_school'
    bank.loc[bank.education == 'basic.6y', 'education'] = 'secondary'
    bank.loc[bank.education == 'basic.9y', 'education'] = 'high_school'
    bank.loc[bank.education == 'university.degree', 'education'] = 'tertiary'
    bank.loc[bank.education == 'basic.4y', 'education'] = 'primary'

    bigml = bigml[['age', 'job', 'marital', 'education']]

    bigml_bank_concat = bigml.append(bank, ignore_index=True)
    bigml_bank_concat.loc[bigml_bank_concat.job == 'admin.', 'job'] = 'admin'

    random_genders = [random.choice(GENDERS) for i in range(bigml_bank_concat.shape[0])]
    bigml_bank_concat['gender'] = random_genders

    bigml_bank_concat['income'] = bigml_bank_concat.apply(
        lambda row: fake_income(row, income_group_by_age_gender, min_income, max_income),
        axis=1
    )

    return bigml_bank_concat


def fake_income(row, lookup_table, min_income, max_income):
    x = (row['age'], row['gender'])
    income_range = range(min_income, max_income + 1)
    ys = lookup_table.get(x, income_range)
    y = random.choice(ys)
    return int(y)


def add_fake_attributes(data):
    fake_data = {
        'use_facebook': [True, False],
        'has_cellphone': [True, False],
        'marketing_type': ['phone_call', 'facebook', 'email', 'poster'],
    }

    for att, value in fake_data.items():
        data[att] = data.apply(
            lambda row: random.choice(value),
            axis=1
        )

    add_fake_name(data)

    return data


def fake_children(customer):
    num_of_childs = random.randint(1, 3)

    fake_child = lambda: {
        'age': random.randint(1, 18),
        'gender': random.choice(GENDERS),
        'grade': random.randint(1, 12),
        'subject_scores': {
            'math': random.choice(SCORE_RANGE),
            'literature': random.choice(SCORE_RANGE),
            'english': random.choice(SCORE_RANGE),
            'physic': random.choice(SCORE_RANGE),
            'chemistry': random.choice(SCORE_RANGE),
        },
    }
    children = [fake_child() for i in range(num_of_childs)]

    customer.update(
        {
            'children': children,
        }
    )
    return customer


def add_fake_name(data):
    return
    # TODO: Update this
    count = data['gender'].value_counts()
    female_count = int(count['female'])
    male_count = int(count['male'])
    female_names = generate_names('female', female_count)
    male_names = generate_names('male', male_count)


def generate_names(gender, count, module_path=None):
    if module_path is None:
        module_path = MODULE_DIR + '/../../external_tools/namegen/namegen.py'

    gender = 'M' if gender == 'male' else 'F'
    args = [
        'python3.6',
        module_path,
        '-o', MODULE_DIR + '/../../tmp/names.txt',
        '-c', str(count),
        '-n', 'Vietnamese',
        '-g', gender
    ]
    subprocess.call(args)

    with open('../tmp/names.txt', encoding='utf-8', mode='r') as name_file:
        names = name_file.readlines()

    names = [name.strip() for name in names]
    return names


@timeit
def generate_dataset(output_file):
    logger.info('Generating base dataset')
    chunksize = 100

    combined_dataset = generate_dataset_from_existing_datasets(DATASETS_DIR)
    fake_attribute_added_dataset = add_fake_attributes(combined_dataset)
    json_dataset = fake_attribute_added_dataset.to_json(orient='records')

    json_dataset = json.loads(json_dataset)

    pool = ProcessingPool()
    json_dataset = pool.map(fake_children, json_dataset, chunksize=chunksize)

    with open(output_file, 'w') as dataset_file:
        json.dump(json_dataset, dataset_file)


def conversion_score(row):
    # TODO: Fix scoring function
    score = (row['income'] + 1) * row['use_facebook'] * \
            (row['marital_married'] + 1) * (row['education_high_school'] + row['education_tertiary'] + 1) + \
            random.random()
    return score


def annotate(data, scoring_function, label_column_name):
    df = pd.DataFrame()
    df[label_column_name] = data.apply(scoring_function, axis=1)
    df[[label_column_name]] = MinMaxScaler().fit_transform(df[[label_column_name]])
    mean = df[label_column_name].mean()
    df.loc[df[label_column_name] > mean, label_column_name] = True
    df.loc[df[label_column_name] <= mean, label_column_name] = False

    df = df.to_dict('records')
    df = [{label_column_name: bool(label[label_column_name])} for label in df]

    return df


@timeit
def annotate_dataset(unannotated_dataset, output_dataset_dir):
    logger.info('Annotating')
    with open(unannotated_dataset, mode='r') as f:
        data = json.load(f)

    preprocessed_data = preprocess_data(data)
    label = annotate(preprocessed_data, conversion_score, 'conversion')
    annotated_data = list(map(merge_dicts, data, label))
    train, test = train_test_split(annotated_data, test_size=0.2)

    try:
        os.makedirs(output_dataset_dir)
    except FileExistsError:
        pass

    with open(output_dataset_dir + '/train.json', mode='w') as f:
        json.dump(train, f)
    with open(output_dataset_dir + '/test.json', mode='w') as f:
        json.dump(test, f)


def upload_dataset_to_mongodb(input_file, mongodb_uri, db_name, collection_name):
    with open(input_file, encoding='utf-8', mode='r') as f:
        data = json.load(f)

    mongo_client = MongoClient(mongodb_uri)
    customer_db = mongo_client[db_name]
    model_train_set_collection = customer_db[collection_name]
    model_train_set_collection.delete_many({})
    model_train_set_collection.insert_many(data)


def merge_dicts(dict1, dict2):
    dict1.update(dict2)
    return dict1
