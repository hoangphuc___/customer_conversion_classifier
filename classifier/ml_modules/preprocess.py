from multiprocessing.pool import Pool

import numpy as np
import pandas as pd
import tqdm
from joblib import dump
from loguru import logger
from sklearn.preprocessing import MinMaxScaler

from ..utils import timeit, flatten_dict


@timeit
def preprocess_data(data, scaler=None, dummy_columns=None, output_scaler_path=None, output_dummy_columns_path=None):
    logger.info('Preprocessing data')

    chunksize = 100
    with Pool() as pool:
        data = list(
            tqdm.tqdm(
                pool.imap(
                    process_sample,
                    data,
                    chunksize=chunksize
                ),
                total=len(data),
                desc='Un-nesting data'
            )
        )

    data = pd.DataFrame(data)

    # Replace boolean
    data['use_facebook'] = data['use_facebook'].map({True: 1, False: 0})
    data['has_cellphone'] = data['has_cellphone'].map({True: 1, False: 0})

    # Replace gender
    data['gender'] = data['gender'].map({'male': 1, 'female': 0})

    # Encoding categorical features
    if dummy_columns is None:
        dummy_columns = pd.get_dummies(data).columns
        if output_dummy_columns_path is not None:
            dump(dummy_columns, output_dummy_columns_path)

    data = encoding_categorical_features(data, dummy_columns)

    # Scaling features
    data = data.astype(float)
    if scaler is None:
        scaler = MinMaxScaler()
        scaler.fit(data.values)
        if output_scaler_path is not None:
            dump(scaler, output_scaler_path)

    scaled_features = scaler.transform(data.values)
    scaled_features_df = pd.DataFrame(scaled_features, index=data.index, columns=data.columns)

    return scaled_features_df


def encoding_categorical_features(data, dummy_columns):
    data = pd.get_dummies(data)
    data = data.reindex(columns=dummy_columns, fill_value=0)
    return data


def process_sample(sample):
    children = sample['children']
    children = list(map(flatten_dict, children))

    # TODO: Not drop children's gender
    children = list(map(pop_child_gender, children))

    keys = ['age', 'grade', 'subject_scores_math', 'subject_scores_literature', 'subject_scores_english',
            'subject_scores_physic', 'subject_scores_chemistry']
    children = dict(zip(keys,
                        np.mean([list(child.values()) for child in children], axis=0)))

    sample['children'] = children

    sample = flatten_dict(sample)

    return sample


def pop_child_gender(child):
    child.pop('gender')
    return child
