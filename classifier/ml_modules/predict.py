from .preprocess import preprocess_data


def predict(model, customer, columns_order):
    scaler = model['scaler']
    clf = model['classifier']
    dummy_columns = model['dummy_columns']

    data = [customer]
    preprocessed_data = preprocess_data(data, scaler=scaler, dummy_columns=dummy_columns)
    preprocessed_data = preprocessed_data[columns_order]
    pred = clf.predict_proba(preprocessed_data)[0].tolist()
    pred = dict(zip(clf.classes_.tolist(), pred))
    return pred
