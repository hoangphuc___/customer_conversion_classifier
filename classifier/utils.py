import collections
import logging
import os
import shutil
import time

from loguru import logger


def configure_logger(module_name):
    formatter = logging.Formatter(fmt='%(asctime)s: %(message)s', datefmt='%m/%d/%Y %I:%M:%S %p')

    handler = logging.StreamHandler()
    handler.setFormatter(fmt=formatter)

    logger = logging.getLogger(module_name)
    logger.addHandler(handler)
    logger.setLevel(logging.DEBUG)
    return logger


def get_module_dir(module_file):
    path = os.path.dirname(os.path.abspath(module_file))
    return path


def timeit(method):
    def timed(*args, **kw):
        ts = time.time()
        result = method(*args, **kw)
        te = time.time()
        if 'log_time' in kw:
            name = kw.get('log_name', method.__name__.upper())
            kw['log_time'][name] = int((te - ts) * 1000)
        else:
            logger.info("%s's elapsed time:  %s s" % \
                        (method.__name__, te - ts))
        return result

    return timed


def is_empty_dir(dir):
    if len(os.listdir(dir)) == 0:
        return True
    else:
        return False


def clear_directory(folder):
    for the_file in os.listdir(folder):
        file_path = os.path.join(folder, the_file)
        try:
            if os.path.isfile(file_path):
                os.unlink(file_path)
            elif os.path.isdir(file_path):
                shutil.rmtree(file_path)
        except Exception as e:
            logger.error(e)


def flatten_dict(d, parent_key='', sep='_'):
    items = []
    for k, v in d.items():
        new_key = parent_key + sep + k if parent_key else k
        if isinstance(v, collections.MutableMapping):
            items.extend(flatten_dict(v, new_key, sep=sep).items())
        else:
            items.append((new_key, v))
    return dict(items)


def without_keys(d, keys):
    new_d = d.copy()
    for key in keys:
        new_d.pop(key)
    return new_d
