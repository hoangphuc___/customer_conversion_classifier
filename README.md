### Prerequisites

- Python 3 (Tested on Python 3.6)

### Installing

- Install packages

```
pip3 install -r requirements.txt
```

Or

- Install package using virtualenv

```
virtualenv --python=python3 <VIRTUALENV_PATH>
source <VIRTUALENV_PATH>/bin/activate
pip install -r requirements.txt
```
- VIRTUALENV_PATH: Path to install virtualenv
## Running the tests

- Set MongoDB connection URI
```
export MONGO_URI=<MongoDB connection URI>
```

- Set MongoDB database name
```
export MONGO_DATABASE_NAME=<MongoDB database name>
```

- Start API server

```
gunicorn test.api.app:app --chdir test -b <HOST>:<PORT>
```

- If using virtualenv
```
source <VIRTUALENV_PATH>/bin/activate
gunicorn test.api.app:app --chdir test -b <HOST>:<PORT>
```