from flask import request, Blueprint, jsonify, current_app, make_response
from loguru import logger
from pymongo import MongoClient
from pymongo.errors import ConnectionFailure, ServerSelectionTimeoutError

import classifier

api_blueprint = Blueprint('api', __name__, url_prefix='/api')


@api_blueprint.route('/predict', methods=['POST'])
def predict_request_handler():
    customer = request.json
    if customer is None:
        response = {
            'error': 'JSON params is required'
        }
        return make_response(jsonify(response), 400)

    validator = current_app.config['predict_sample_validator']
    validation_result = validator.validate(customer)
    if not validation_result:
        response = {
            'error': validator.errors
        }
        return make_response(jsonify(response), 400)

    customer_order = current_app.config['sample_order']

    logger.debug(customer)

    model = current_app.config['model']
    predict_result = classifier.predict(model, customer, customer_order)

    response = {
        'conversion': predict_result
    }

    logger.debug(response)
    return jsonify(response)


@api_blueprint.route('/datasets', methods=['POST'])
def add_train_sample():
    customer = request.json
    params = request.args

    try:
        dataset_type = params['dataset_type']
        if dataset_type not in ['train', 'test']:
            response = {
                'error': "'dataset_type' value must in ['train', 'test']"
            }
            return make_response(jsonify(response), 400)
    except KeyError:
        response = {
            'error': "'dataset_type' param is required"
        }
        return make_response(jsonify(response), 400)

    validator = current_app.config['dataset_sample_validator']
    validation_result = validator.validate(customer)
    if not validation_result:
        response = {
            'error': validator.errors
        }
        return make_response(jsonify(response), 400)

    try:
        mongo_uri = current_app.config['mongo_uri']
        mongo_database_name = current_app.config['mongo_database_name']
        mongo_client = MongoClient(mongo_uri)
        customer_db = mongo_client[mongo_database_name]
        model_train_set_collection = customer_db[dataset_type]
        model_train_set_collection.insert_one(customer)
    except (ConnectionFailure, ServerSelectionTimeoutError):
        raise Exception('Can not connect to MongoDB')

    response = {
        'message': 'success'
    }
    return jsonify(response)


@api_blueprint.route('/retrain', methods=['POST'])
def trigger_retrain_models():
    retrain_thread = current_app.config['retrain_thread']
    if retrain_thread.isAlive():
        response = {
            'message': 'The training process is currently running. Please try again later'
        }
        return make_response(jsonify(response), 202)
    else:
        retrain_thread.start()

        response = {
            'message': 'success'
        }
        return make_response(jsonify(response), 202)


@api_blueprint.route('/evaluate', methods=['GET'])
def evaluate_models():
    model = current_app.config['model']

    try:
        mongo_uri = current_app.config['mongo_uri']
        mongo_database_name = current_app.config['mongo_database_name']
        sample_order = current_app.config['sample_order']

        mongo_client = MongoClient(mongo_uri)
        customer_db = mongo_client[mongo_database_name]
        model_train_set_collection = customer_db['test']
        mongodb_cursor = model_train_set_collection.find({}, {'_id': False})

        evaluate_result = classifier.evaluate_model(model, sample_order, 'conversion',
                                                    mongodb_cursor=mongodb_cursor)

        # TODO: Check exception type
    except (ConnectionFailure, ServerSelectionTimeoutError):
        raise Exception('Can not connect to MongoDB')

    response = {
        'evaluation_results': evaluate_result
    }
    return jsonify(response)


@api_blueprint.route('/analyze_dataset', methods=['POST'])
def analyze_dataset():
    analysis_thread = current_app.config['analysis_thread']
    if analysis_thread.isAlive():
        response = {
            'message': 'The training process is currently running. Please try again later'
        }
        return make_response(jsonify(response), 202)
    else:
        current_app.config['dataset_statistic'] = None
        analysis_thread.start()

        response = {
            'message': 'success'
        }
        return make_response(jsonify(response), 202)


@api_blueprint.route('/dataset_statistic', methods=['GET'])
def dataset_statistic():
    dataset_statistic = current_app.config.get('dataset_statistic', None)
    if dataset_statistic is None:
        response = {
            'message': 'Dataset statistic is currently not available'
        }

        return jsonify(response), 204
    else:
        response = dataset_statistic

        return jsonify(response)
