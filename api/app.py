import os
import threading

from cerberus import Validator
from flask import Flask, current_app
from joblib import load
from loguru import logger
from pymongo import MongoClient
from pymongo.errors import ConnectionFailure, ServerSelectionTimeoutError

import classifier
from .request_handlers import api_blueprint
from .sample_order import SAMPLE_ORDER
from .utils import timeit
from .validation_schemas import schemas


@timeit
def load_data():
    logger.info('Loading data')
    model_dir = current_app.config['model_dir']

    model = load_model(model_dir)

    predict_param_schema = schemas['predict_sample_schema']
    predict_sample_validator = Validator(predict_param_schema)

    dataset_sample_validator = schemas['dataset_sample_schema']
    dataset_sample_validator = Validator(dataset_sample_validator)

    try:
        mongo_uri = os.environ['MONGO_URI']
    except KeyError:
        raise EnvironmentError("'MONGO_URL' is not set")

    try:
        mongo_database_name = os.environ['MONGO_DATABASE_NAME']
    except KeyError:
        raise EnvironmentError("'MONGO_DATABASE_NAME' is not set")

    app_obj = current_app._get_current_object()
    retrain_thread = threading.Thread(
        target=retrain_model,
        args=(model_dir, SAMPLE_ORDER, mongo_uri, mongo_database_name, app_obj)
    )
    analysis_thread = threading.Thread(
        target=dataset_analysis_task,
        args=(app_obj,)
    )

    current_app.config.update(
        {
            'model': model,
            'predict_sample_validator': predict_sample_validator,
            'dataset_sample_validator': dataset_sample_validator,
            'sample_order': SAMPLE_ORDER,
            'mongo_uri': mongo_uri,
            'retrain_thread': retrain_thread,
            'analysis_thread': analysis_thread,
            'mongo_database_name': mongo_database_name,
        }
    )


def load_model(dir):
    # TODO: Train model if model does not exist
    scaler = load(dir + '/scaler.joblib')
    classifier = load(dir + '/classifier.joblib')
    dummy_columns = load(dir + '/dummy_columns.joblib')

    model = {
        'scaler': scaler,
        'classifier': classifier,
        'dummy_columns': dummy_columns,
    }
    return model


def setup_app(model_dir):
    app = Flask(__name__)
    app.config.update({
        'model_dir': model_dir,
    })
    app.register_blueprint(api_blueprint)
    app.before_first_request(load_data)
    return app


@timeit
def retrain_model(model_dir, sample_order, mongo_uri, mongo_database_name, app_obj):
    try:
        mongo_client = MongoClient(mongo_uri)
        customer_db = mongo_client[mongo_database_name]
        model_train_set_collection = customer_db['train']
        mongodb_cursor = model_train_set_collection.find({}, {'_id': False})
        classifier.train_model(model_dir, sample_order, 'conversion', mongodb_cursor=mongodb_cursor)

        app_obj.config.update({
            'model': load_model(model_dir),
        })
        # TODO: Check exception type
    except (ConnectionFailure, ServerSelectionTimeoutError):
        raise Exception('Can not connect to MongoDB')


@timeit
def dataset_analysis_task(app_obj):
    try:
        mongo_uri = app_obj.config['mongo_uri']
        mongo_database_name = app_obj.config['mongo_database_name']

        mongo_client = MongoClient(mongo_uri)
        customer_db = mongo_client[mongo_database_name]
        model_train_set_collection = customer_db['train']
        model_test_set_collection = customer_db['test']

        train_cursor = model_train_set_collection.find({}, {'_id': False})
        test_cursor = model_test_set_collection.find({}, {'_id': False})
        data = list(train_cursor) + list(test_cursor)
        dataset_statistics = classifier.analyze_dataset(data)

        app_obj.config.update(
            {
                'dataset_statistic': dataset_statistics
            }
        )

        # TODO: Check exception type
    except (ConnectionFailure, ServerSelectionTimeoutError):
        raise Exception('Can not connect to MongoDB')
