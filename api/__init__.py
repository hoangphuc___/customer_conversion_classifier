from .app import setup_app

__all__ = ['setup_app']
