import time

from loguru import logger


def timeit(method):
    def timed(*args, **kw):
        ts = time.time()
        result = method(*args, **kw)
        te = time.time()
        if 'log_time' in kw:
            name = kw.get('log_name', method.__name__.upper())
            kw['log_time'][name] = int((te - ts) * 1000)
        else:
            logger.info("%s's elapsed time:  %s s" % \
                        (method.__name__, te - ts))
        return result

    return timed


def sort_dict_by_list(target_dict, order):
    index_map = {v: i for i, v in enumerate(order)}
    target_dict = sorted(target_dict.items(), key=lambda pair: index_map[pair[0]])
    target_dict = {item[0]: item[1] for item in target_dict}
    return target_dict
