import copy

gender_rule = {
    'allowed': ['male', 'female'],
    'required': True,
}
predict_schema = {
    'age': {
        'type': 'integer',
        'min': 1,
        'max': 150,
        'required': True,
    },
    'job': {
        'allowed': ['retired', 'unknown', 'services', 'technician', 'housemaid', 'student', 'admin', 'management',
                    'self-employed', 'entrepreneur', 'unemployed', 'blue-collar'],
        'required': True,
    },
    'marital': {
        'allowed': ['married', 'single', 'divorced', 'unknown'],
        'required': True,
    },
    'education': {
        'allowed': ['unknown', 'primary', 'illiterate', 'secondary', 'high_school', 'tertiary'],
        'required': True,
    },
    'gender': gender_rule,
    'income': {
        'type': 'integer',
        'min': 0,
        'required': True,
    },
    'children': {
        'type': 'list',
        'schema': {
            'type': 'dict',
            'schema': {
                'age': {
                    'type': 'integer',
                    'min': 1,
                    'max': 18,
                    'required': True,
                },
                'gender': gender_rule,
                'grade': {
                    'type': 'integer',
                    'min': 1,
                    'max': 12,
                    'required': True,
                },
                'subject_scores': {
                    'type': 'dict',
                    'schema': {
                        'math': {
                            'type': 'float',
                            'min': 0,
                            'max': 10,
                            'required': True,
                        },
                        'literature': {
                            'type': 'float',
                            'min': 0,
                            'max': 10,
                            'required': True,
                        },
                        'english': {
                            'type': 'float',
                            'min': 0,
                            'max': 10,
                            'required': True,
                        },
                        'physic': {
                            'type': 'float',
                            'min': 0,
                            'max': 10,
                            'required': True,
                        },
                        'chemistry': {
                            'type': 'float',
                            'min': 0,
                            'max': 10,
                            'required': True,
                        },
                    },
                    'required': True,
                }
            }
        },
        'required': True,
    },
    'use_facebook': {
        'type': 'boolean',
    },
    'has_cellphone': {
        'type': 'boolean',
    },
    'marketing_type': {
        'allowed': ['phone_call', 'facebook', 'email', 'poster'],
        'required': True,
    }
}

dataset_sample_schema = copy.deepcopy(predict_schema)
dataset_sample_schema.update(
    {
        'conversion': {
            'type': 'boolean',
        }
    }
)
schemas = {
    'predict_sample_schema': predict_schema,
    'dataset_sample_schema': dataset_sample_schema,
}
