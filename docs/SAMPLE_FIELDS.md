- Current fields
```
{
    'age': {
        'type': 'integer',
        'min': 1,
        'max': 150,
        'required': True,
    },
    'job': {
        'allowed': ['retired', 'unknown', 'services', 'technician', 'housemaid', 'student', 'admin', 'management',
                    'self-employed', 'entrepreneur', 'unemployed', 'blue-collar'],
        'required': True,
    },
    'marital': {
        'allowed': ['married', 'single', 'divorced', 'unknown'],
        'required': True,
    },
    'education': {
        'allowed': ['unknown', 'primary', 'illiterate', 'secondary', 'high_school', 'tertiary'],
        'required': True,
    },
    'gender': {
        'allowed': ['male', 'female'],
        'required': True,
    },
    'income': {
        'type': 'integer',
        'min': 0,
        'required': True,
    },
    'children': {
        'type': 'integer',
        'min': 0,
        'required': True,
    },
    'use_facebook': {
        'allowed': ['true', 'false'],
        'coerce': str.lower,
        'required': True,
    },
    'has_cellphone': {
        'allowed': ['true', 'false'],
        'coerce': str.lower,
        'required': True,
    },
    'conversion': {
        'allowed': ['true', 'false'],
        'coerce': str.lower,
        'required': True,
    }
}
```
- To be implemented
    - Center's address
    - Address
      - How did you hear about us?
      - Children 
        - Age
        - Gender
        - Grade
        - Scores
            - Math
            - Literature
            - English