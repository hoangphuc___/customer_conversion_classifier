BASE_URL: https://customer-conversion-classifier.herokuapp.com
# Predict conversion of customer
* Method: POST
* URL: <BASE_URL>/api/predict
* Request:
    * JSON: 
        ```
        {
            "age": 32,
            "job": "services",
            "marital": "married",
            "education": "high_school",
            "gender": "male",
            "income": 75,
            "use_facebook": false,
            "has_cellphone": true,
            "marketing_type": "email",
            "children": [
                {
                    "age": 2,
                    "gender": "female",
                    "grade": 5,
                    "subject_scores": {
                        "math": 9.75,
                        "literature": 5,
                        "english": 10,
                        "physic": 7.75,
                        "chemistry": 4.25
                    }
                }
            ]
        }
        ```
* Response: 
    * JSON:
        ```
        {
            "conversion": {
                "false": 0.9202675550314969,
                "true": 0.07973244496850315
            }
        }
        ```

* cURL example:
    ```
    curl -X POST \
      https://customer-conversion-classifier.herokuapp.com/api/predict \
      -H 'Content-Type: application/json' \
      -d '{
        "age": 32,
        "job": "services",
        "marital": "married",
        "education": "high_school",
        "gender": "male",
        "income": 75,
        "use_facebook": false,
        "has_cellphone": true,
        "marketing_type": "email",
        "children": [
            {
                "age": 2,
                "gender": "female",
                "grade": 5,
                "subject_scores": {
                    "math": 9.75,
                    "literature": 5,
                    "english": 10,
                    "physic": 7.75,
                    "chemistry": 4.25
                }
            }
        ]
    }'
    ```

# Add new sample to dataset
* Method: POST
* URL: <BASE_URL>/api/dataset
* Request:
    * Params:
        * dataset_type: Type of dataset. Possible value: train, test
    * JSON: 
        ```
        {
            "age": 32,
            "job": "services",
            "marital": "married",
            "education": "high_school",
            "gender": "male",
            "income": 75,
            "use_facebook": false,
            "has_cellphone": true,
            "marketing_type": "email",
            "children": [
                {
                    "age": 2,
                    "gender": "female",
                    "grade": 5,
                    "subject_scores": {
                        "math": 9.75,
                        "literature": 5,
                        "english": 10,
                        "physic": 7.75,
                        "chemistry": 4.25
                    }
                }
            ],
            "conversion": true
        }
        ```
* Response: 
    * JSON: 
        ```
        {
            "message": "success"
        }
        ```
* cURL example:
    ```
    curl -X POST \
      'https://customer-conversion-classifier.herokuapp.com/api/datasets?dataset_type=test' \
      -H 'Content-Type: application/json' \
      -d '{
        "age": 32,
        "job": "services",
        "marital": "married",
        "education": "high_school",
        "gender": "male",
        "income": 75,
        "use_facebook": false,
        "has_cellphone": true,
        "marketing_type": "email",
        "children": [
            {
                "age": 2,
                "gender": "female",
                "grade": 5,
                "subject_scores": {
                    "math": 9.75,
                    "literature": 5,
                    "english": 10,
                    "physic": 7.75,
                    "chemistry": 4.25
                }
            }
        ],
        "conversion": true
    }'
    ```

# Retrain model
* Method: POST
* URL: <BASE_URL>/api/retrain
* Response: 
    * JSON: 
        ```
        {
            "message": "success"
        }
        ```
* cURL example:
    ```
    curl -X POST \
      https://customer-conversion-classifier.herokuapp.com/api/retrain \
    ```

# Evaluate model
* Method: GET
* URL: <BASE_URL>/api/evaluate
* Response: 
    * JSON: 
        ```
        {
            "evaluation_results": {
                "accuracy": 0.9799780105317979,
                "f1": 0.9795269662292558,
                "precision": 0.9801694827390425,
                "recall": 0.9789228031198667
            }
        }
        ```
* cURL example:
    ```
    curl -X GET \
      https://customer-conversion-classifier.herokuapp.com/api/evaluate \
    ```

# Analyze dataset
* Method: POST
* URL: <BASE_URL>/api/analyze_dataset
* Response: 
    * JSON: 
        ```
        {
            "message": "success"
        }
        ```
* cURL example:
    ```
    curl -X POST \
      https://customer-conversion-classifier.herokuapp.com/api/analyze_dataset \
    ```

# Get dataset statistic
* Method: GET
* URL: <BASE_URL>/api/analyze_dataset
* Response: 
    * JSON: 
        ```
        {
            "email": {
                "columns": {
                    "age": {
                        "17-24": 0.014576018725396319,
                        "25-34": 0.3223747207149697,
                        "35-44": 0.32280029790403236,
                        "45-54": 0.22395999574422812,
                        "55-64": 0.0989466964570699,
                        "65+": 0.01734227045430365
                    },
                    "children_age": {
                        "11-14": 0.2624747313543994,
                        "15-17": 0.09618044472816258,
                        "18+": 0.018193424832428983,
                        "5-": 0.18874348334929247,
                        "6-10": 0.4344079157357166
                    },
                    "children_grade": {
                        "1-5": 0.38450899031811897,
                        "10-12": 0.13022661985317588,
                        "6-9": 0.4852643898287052
                    },
                    "children_subject_scores_chemistry": {
                        "3.5-": 0.2647090115969784,
                        "3.5-5": 0.2313012022555591,
                        "5-6.5": 0.22725821895946377,
                        "6.5-8": 0.15895308011490583,
                        "8+": 0.11777848707309288
                    },
                    "children_subject_scores_english": {
                        "3.5-": 0.25917650813916376,
                        "3.5-5": 0.23055644217469945,
                        "5-6.5": 0.23374827109266944,
                        "6.5-8": 0.16108096606021918,
                        "8+": 0.11543781253324822
                    },
                    "children_subject_scores_literature": {
                        "3.5-": 0.2629003085434621,
                        "3.5-5": 0.22576869879774444,
                        "5-6.5": 0.2351313969571231,
                        "6.5-8": 0.15735716565592084,
                        "8+": 0.11884243004574954
                    },
                    "children_subject_scores_math": {
                        "3.5-": 0.2627939142461964,
                        "3.5-5": 0.2290669220129801,
                        "5-6.5": 0.22566230450047878,
                        "6.5-8": 0.1590594744121715,
                        "8+": 0.12341738482817322
                    },
                    "children_subject_scores_physic": {
                        "3.5-": 0.2609852111926801,
                        "3.5-5": 0.23279072241727844,
                        "5-6.5": 0.2344930311735291,
                        "6.5-8": 0.16097457176295352,
                        "8+": 0.11075646345355888
                    },
                    "education": {
                        "high_school": 0.20448983934461112,
                        "illiterate": 0.00021278859453133312,
                        "primary": 0.11426747526332588,
                        "secondary": 0.23779125438876475,
                        "tertiary": 0.41142674752633257,
                        "unknown": 0.0318118948824343
                    },
                    "gender": {
                        "female": 0.5000531971486328,
                        "male": 0.49994680285136717
                    },
                    "has_cellphone": {
                        "false": 0.48664751569315884,
                        "true": 0.5133524843068411
                    },
                    "income": {
                        "120+": 0.05628258325353761,
                        "30-": 0.12363017342270455,
                        "30-60": 0.22055537823172677,
                        "60-90": 0.44451537397595486,
                        "90-120": 0.15501649111607618
                    },
                    "job": {
                        "admin": 0.18374295137780616,
                        "blue-collar": 0.21236301734227045,
                        "entrepreneur": 0.030109586126183637,
                        "housemaid": 0.025534631343759974,
                        "management": 0.16267688051920418,
                        "retired": 0.044153633365251625,
                        "self-employed": 0.0385147356101713,
                        "services": 0.08820087243323758,
                        "student": 0.013831258644536652,
                        "technician": 0.16789020108522182,
                        "unemployed": 0.026704968613682305,
                        "unknown": 0.006277263538674327
                    },
                    "marital": {
                        "divorced": 0.08277476327268858,
                        "married": 0.6899670177678476,
                        "single": 0.22672624747313544,
                        "unknown": 0.0005319714863283328
                    },
                    "use_facebook": {
                        "false": 0.00010639429726566656,
                        "true": 0.9998936057027343
                    }
                },
                "summary": 0.25106849022331446
            },
            "facebook": {
                "columns": {
                    "age": {
                        "17-24": 0.011935483870967743,
                        "25-34": 0.3120430107526882,
                        "35-44": 0.3368817204301075,
                        "45-54": 0.2253763440860215,
                        "55-64": 0.09688172043010752,
                        "65+": 0.016881720430107525
                    },
                    "children_age": {
                        "11-14": 0.270752688172043,
                        "15-17": 0.09,
                        "18+": 0.019139784946236558,
                        "5-": 0.18655913978494623,
                        "6-10": 0.4335483870967742
                    },
                    "children_grade": {
                        "1-5": 0.39483870967741935,
                        "10-12": 0.13440860215053763,
                        "6-9": 0.470752688172043
                    },
                    "children_subject_scores_chemistry": {
                        "3.5-": 0.2649462365591398,
                        "3.5-5": 0.2286021505376344,
                        "5-6.5": 0.2310752688172043,
                        "6.5-8": 0.15913978494623657,
                        "8+": 0.11623655913978495
                    },
                    "children_subject_scores_english": {
                        "3.5-": 0.25989247311827957,
                        "3.5-5": 0.23376344086021505,
                        "5-6.5": 0.23236559139784946,
                        "6.5-8": 0.1532258064516129,
                        "8+": 0.12075268817204302
                    },
                    "children_subject_scores_literature": {
                        "3.5-": 0.2593548387096774,
                        "3.5-5": 0.23236559139784946,
                        "5-6.5": 0.23193548387096774,
                        "6.5-8": 0.15473118279569892,
                        "8+": 0.12161290322580645
                    },
                    "children_subject_scores_math": {
                        "3.5-": 0.25967741935483873,
                        "3.5-5": 0.23709677419354838,
                        "5-6.5": 0.2252688172043011,
                        "6.5-8": 0.15956989247311829,
                        "8+": 0.11838709677419355
                    },
                    "children_subject_scores_physic": {
                        "3.5-": 0.25709677419354837,
                        "3.5-5": 0.2270967741935484,
                        "5-6.5": 0.24086021505376345,
                        "6.5-8": 0.1543010752688172,
                        "8+": 0.12064516129032259
                    },
                    "education": {
                        "high_school": 0.20720430107526883,
                        "illiterate": 0.00021505376344086021,
                        "primary": 0.12322580645161291,
                        "secondary": 0.23010752688172043,
                        "tertiary": 0.40795698924731183,
                        "unknown": 0.03129032258064516
                    },
                    "gender": {
                        "female": 0.5002150537634409,
                        "male": 0.4997849462365591
                    },
                    "has_cellphone": {
                        "false": 0.5035483870967742,
                        "true": 0.4964516129032258
                    },
                    "income": {
                        "120+": 0.05473118279569893,
                        "30-": 0.13623655913978494,
                        "30-60": 0.21301075268817204,
                        "60-90": 0.4388172043010753,
                        "90-120": 0.1572043010752688
                    },
                    "job": {
                        "admin": 0.17709677419354838,
                        "blue-collar": 0.21989247311827956,
                        "entrepreneur": 0.033118279569892474,
                        "housemaid": 0.026881720430107527,
                        "management": 0.16268817204301075,
                        "retired": 0.04967741935483871,
                        "self-employed": 0.03634408602150538,
                        "services": 0.08623655913978495,
                        "student": 0.014946236559139785,
                        "technician": 0.16344086021505377,
                        "unemployed": 0.023118279569892472,
                        "unknown": 0.006559139784946237
                    },
                    "marital": {
                        "divorced": 0.08258064516129032,
                        "married": 0.6886021505376344,
                        "single": 0.2281720430107527,
                        "unknown": 0.0006451612903225806
                    },
                    "use_facebook": {
                        "true": 1
                    }
                },
                "summary": 0.2484239769206112
            },
            "phone_call": {
                "columns": {
                    "age": {
                        "17-24": 0.01457233368532207,
                        "25-34": 0.3135163674762408,
                        "35-44": 0.3287222808870116,
                        "45-54": 0.22449841605068638,
                        "55-64": 0.10348468848996832,
                        "65+": 0.015205913410770855
                    },
                    "children_age": {
                        "11-14": 0.26029567053854274,
                        "15-17": 0.0955649419218585,
                        "18+": 0.021752903907074975,
                        "5-": 0.19683210137275609,
                        "6-10": 0.42555438225976766
                    },
                    "children_grade": {
                        "1-5": 0.3955649419218585,
                        "10-12": 0.1365364308342133,
                        "6-9": 0.46789862724392817
                    },
                    "children_subject_scores_chemistry": {
                        "3.5-": 0.2573389651531151,
                        "3.5-5": 0.23336853220696938,
                        "5-6.5": 0.23305174234424497,
                        "6.5-8": 0.15850052798310454,
                        "8+": 0.117740232312566
                    },
                    "children_subject_scores_english": {
                        "3.5-": 0.251636747624076,
                        "3.5-5": 0.22882787750791975,
                        "5-6.5": 0.2304118268215417,
                        "6.5-8": 0.164519535374868,
                        "8+": 0.1246040126715945
                    },
                    "children_subject_scores_literature": {
                        "3.5-": 0.25818373812038015,
                        "3.5-5": 0.22819429778247097,
                        "5-6.5": 0.23738120380147834,
                        "6.5-8": 0.15543822597676873,
                        "8+": 0.1208025343189018
                    },
                    "children_subject_scores_math": {
                        "3.5-": 0.26124604012671593,
                        "3.5-5": 0.23115100316789863,
                        "5-6.5": 0.22692713833157338,
                        "6.5-8": 0.15776135163674762,
                        "8+": 0.12291446673706441
                    },
                    "children_subject_scores_physic": {
                        "3.5-": 0.25649419218585007,
                        "3.5-5": 0.22597676874340022,
                        "5-6.5": 0.23590285110876452,
                        "6.5-8": 0.16462513199577614,
                        "8+": 0.11700105596620908
                    },
                    "education": {
                        "high_school": 0.20485744456177402,
                        "illiterate": 0.00021119324181626187,
                        "primary": 0.12312565997888067,
                        "secondary": 0.23505807814149948,
                        "tertiary": 0.40464625131995774,
                        "unknown": 0.032101372756071805
                    },
                    "gender": {
                        "female": 0.5025343189017951,
                        "male": 0.4974656810982049
                    },
                    "has_cellphone": {
                        "false": 0.49060190073917637,
                        "true": 0.5093980992608237
                    },
                    "income": {
                        "120+": 0.05216473072861669,
                        "30-": 0.1316789862724393,
                        "30-60": 0.2273495248152059,
                        "60-90": 0.44076029567053854,
                        "90-120": 0.14804646251319958
                    },
                    "job": {
                        "admin": 0.18479408658922913,
                        "blue-collar": 0.21214361140443505,
                        "entrepreneur": 0.038965153115100314,
                        "housemaid": 0.026610348468848997,
                        "management": 0.15923970432946147,
                        "retired": 0.046990496304118265,
                        "self-employed": 0.035480464625132,
                        "services": 0.09102428722280886,
                        "student": 0.01351636747624076,
                        "technician": 0.15860612460401266,
                        "unemployed": 0.02481520591341077,
                        "unknown": 0.00781414994720169
                    },
                    "marital": {
                        "divorced": 0.08162618796198522,
                        "married": 0.6969376979936642,
                        "single": 0.22027455121436115,
                        "unknown": 0.0011615628299894403
                    },
                    "use_facebook": {
                        "true": 1
                    }
                },
                "summary": 0.2529650603696976
            },
            "poster": {
                "columns": {
                    "age": {
                        "17-24": 0.016618107262328694,
                        "25-34": 0.31347793244847305,
                        "35-44": 0.32923276141146,
                        "45-54": 0.22391280889176648,
                        "55-64": 0.10089565123556707,
                        "65+": 0.015862738750404663
                    },
                    "children_age": {
                        "11-14": 0.26794000215819574,
                        "15-17": 0.09668716952627604,
                        "18+": 0.019100032372936226,
                        "5-": 0.1878709398942484,
                        "6-10": 0.4284018560483436
                    },
                    "children_grade": {
                        "1-5": 0.3892306032157117,
                        "10-12": 0.1338081363979713,
                        "6-9": 0.47696126038631703
                    },
                    "children_subject_scores_chemistry": {
                        "3.5-": 0.25865976044027195,
                        "3.5-5": 0.23394841912161432,
                        "5-6.5": 0.23459587784612065,
                        "6.5-8": 0.1557138232437682,
                        "8+": 0.11708211934822488
                    },
                    "children_subject_scores_english": {
                        "3.5-": 0.26146541491313263,
                        "3.5-5": 0.22337325995467788,
                        "5-6.5": 0.23794108125606991,
                        "6.5-8": 0.15895111686629979,
                        "8+": 0.11826912700981979
                    },
                    "children_subject_scores_literature": {
                        "3.5-": 0.25747275277867704,
                        "3.5-5": 0.23297723103485485,
                        "5-6.5": 0.23524333657062696,
                        "6.5-8": 0.1595985755908061,
                        "8+": 0.11470810402503508
                    },
                    "children_subject_scores_math": {
                        "3.5-": 0.2635157008740693,
                        "3.5-5": 0.23006366677457646,
                        "5-6.5": 0.23135858422358907,
                        "6.5-8": 0.15668501133052767,
                        "8+": 0.1183770367972375
                    },
                    "children_subject_scores_physic": {
                        "3.5-": 0.2621128736376389,
                        "3.5-5": 0.23103485486133593,
                        "5-6.5": 0.22132297399374123,
                        "6.5-8": 0.16520988453652746,
                        "8+": 0.12031941297075645
                    },
                    "education": {
                        "high_school": 0.20751052120427324,
                        "illiterate": 0.00021581957483543757,
                        "primary": 0.11384482572569332,
                        "secondary": 0.23750944210639904,
                        "tertiary": 0.4077910866515593,
                        "unknown": 0.03312830473723967
                    },
                    "gender": {
                        "female": 0.4981115787201899,
                        "male": 0.50188842127981
                    },
                    "has_cellphone": {
                        "false": 0.4929319089241394,
                        "true": 0.5070680910758606
                    },
                    "income": {
                        "120+": 0.05881083414265674,
                        "30-": 0.13111039171252833,
                        "30-60": 0.22153879356857667,
                        "60-90": 0.43336570626955867,
                        "90-120": 0.15517427430667963
                    },
                    "job": {
                        "admin": 0.1797777058379195,
                        "blue-collar": 0.2120427322758174,
                        "entrepreneur": 0.03820006474587245,
                        "housemaid": 0.02406388259415129,
                        "management": 0.1585194777166289,
                        "retired": 0.04618538901478364,
                        "self-employed": 0.03830797453329017,
                        "services": 0.08924139419445344,
                        "student": 0.01165425704111363,
                        "technician": 0.16639689219812237,
                        "unemployed": 0.027301176216682852,
                        "unknown": 0.008309053631164347
                    },
                    "marital": {
                        "divorced": 0.07845041545268155,
                        "married": 0.6942915722456027,
                        "single": 0.22661055357720944,
                        "unknown": 0.0006474587245063128
                    },
                    "use_facebook": {
                        "true": 1
                    }
                },
                "summary": 0.24754247248637676
            }
        }
        ```
* cURL example:
    ```
    curl -X GET \
      https://customer-conversion-classifier.herokuapp.com/api/dataset_statistic \
    ```