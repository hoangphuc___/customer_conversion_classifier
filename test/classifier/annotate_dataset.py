import classifier

UNANNOTATED_DATASET_FILE = 'data/datasets/unannotated_dataset.json'
ANNOTATED_DATASET_DIR = 'data/datasets/annotated_dataset'

if __name__ == '__main__':
    classifier.annotate_dataset(
        UNANNOTATED_DATASET_FILE,
        ANNOTATED_DATASET_DIR,
    )
