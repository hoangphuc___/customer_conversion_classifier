from loguru import logger

import classifier

TESTING_SET = 'data/datasets/annotated_dataset/test.json'
MODEL_DIR = 'data/model'

if __name__ == '__main__':
    sample_order = ['age', 'children_age', 'children_grade', 'children_subject_scores_chemistry',
                    'children_subject_scores_english', 'children_subject_scores_literature',
                    'children_subject_scores_math', 'children_subject_scores_physic', 'gender',
                    'has_cellphone', 'income', 'use_facebook', 'education_high_school', 'education_illiterate',
                    'education_primary', 'education_secondary', 'education_tertiary', 'education_unknown', 'job_admin',
                    'job_blue-collar', 'job_entrepreneur', 'job_housemaid', 'job_management', 'job_retired',
                    'job_self-employed', 'job_services', 'job_student', 'job_technician', 'job_unemployed',
                    'job_unknown', 'marital_divorced', 'marital_married', 'marital_single', 'marital_unknown',
                    'marketing_type_email', 'marketing_type_facebook', 'marketing_type_phone_call',
                    'marketing_type_poster']
    evaluation_result = classifier.evaluate_model(MODEL_DIR, sample_order, 'conversion', TESTING_SET)
    logger.info('Evaluation result: %s' % evaluation_result)
