import classifier

OUTPUT_DATASET_FILE = 'data/datasets/unannotated_dataset.json'

if __name__ == '__main__':
    classifier.generate_dataset(OUTPUT_DATASET_FILE)
