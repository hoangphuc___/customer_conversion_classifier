import classifier

MODELS_DIR = 'data/datasets/annotated_dataset'
MONGO_URI = 'mongodb://user0:ka24YP6hZZypYsaa@cluster0-shard-00-00-0ewag.mongodb.net:27017,cluster0-shard-00-01-0ewag.mongodb.net:27017,cluster0-shard-00-02-0ewag.mongodb.net:27017/test?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin&retryWrites=true'
DB_NAME = 'customer'


if __name__ == '__main__':
    classifier.upload_dataset_to_mongodb(MODELS_DIR + '/train.json', MONGO_URI, DB_NAME,
                                         'train')
    classifier.upload_dataset_to_mongodb(MODELS_DIR + '/test.json', MONGO_URI, DB_NAME, 'test')
