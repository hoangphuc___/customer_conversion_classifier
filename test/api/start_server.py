import api

HOST = 'localhost'
PORT = 2020
MODEL_DIR = 'data/model'

app = api.setup_app(MODEL_DIR)

app.run(HOST, PORT, debug=True)
