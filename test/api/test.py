import json

import requests
from loguru import logger

from classifier.utils import without_keys

TEST_SET = 'data/datasets/annotated_dataset/test.json'
URL = 'https://customer-conversion-classifier.herokuapp.com/api/predict'


def test(test_set, num_of_first_sample=None):
    with open(test_set, mode='r') as f:
        test = json.load(f)

    if num_of_first_sample is not None:
        test = test[:num_of_first_sample]

    y_test = [sample['conversion'] for sample in test]
    x_test = [without_keys(sample, ['conversion']) for sample in test]
    for i in range(len(x_test)):
        params = x_test[i]

        response = requests.post(URL, json=params)
        json_response = response.json()
        pred = json_response['conversion']
        truth = y_test[i]
        logger.info('pred: %s - truth: %s' % (pred, truth))


test(TEST_SET, 50)
